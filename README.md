# SpaceInvaders.js
By Bernhard Ruckenstuhl and Markus Exler

# About
This project was a game project for one of the courses (Web Development) in our Bachelors University. All the graphics and sounds used do not belong to us.

# Features
* Fully playable Space Invaders (in browser)
* Local Multiplayer (on the same machine)
* GamePad Support for FireFox
* Music/Sound Effects

# Bugs and missing Features
* Destroyable Shield
* Responsiveness accross different platforms

# Screenshots
### Menu
![Menu Screenshot](https://bitbucket.org/Zumpel96/space-invaders.js/raw/387971ef3ab436f3844847ab8afe26804fb8b44f/screenshots/menu.png)

### Ingame
![Ingame Screenshot](https://bitbucket.org/Zumpel96/space-invaders.js/raw/387971ef3ab436f3844847ab8afe26804fb8b44f/screenshots/gameplay.png)

# Credits 

* [Explosion Sound A] - https://www.freesound.org/people/LittleRobotSoundFactory/sounds/270332/
* [Explosion Sound B] - https://www.freesound.org/people/LittleRobotSoundFactory/sounds/270311/
* [Shoot Sound] - https://www.freesound.org/people/LittleRobotSoundFactory/sounds/270344/
* [Background Music] - https://www.youtube.com/watch?v=BO3XLE_eRPk
* [Logo] - https://de.wikipedia.org/wiki/Datei:Space_invaders_logo.svg
* [Sprites] - https://www.deviantart.com/gooperblooper22/art/Space-Invaders-Sprite-Sheet-135338373